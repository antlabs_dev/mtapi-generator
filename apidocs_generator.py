#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import jinja2
import subprocess
import sys
import traceback
import os
import shutil

try:
    import config as CONF
except ImportError as e:
    sys.exit(u"Error. No config file or there are errors in it\n {m}\ncp config.py.example config.py and edit it\n".format(m=traceback.format_exc()))
    
JINJA2_ENV = jinja2.Environment(loader=jinja2.PackageLoader("apidocs_generator", "templates"))

def call(cli, cwd="/"):
    popen_cli = filter(lambda x : True if len(x) > 0 else False, cli.split(" "))
    process = subprocess.Popen(popen_cli, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd)
    out, err = process.communicate()
    if process.returncode != 0:
        raise RuntimeError(u"Process '{c}' failed with code '{r}' in working directory '{d}'\nstderr: '{e}'\nstdout: '{o}'".\
                           format(c=cli, r=process.returncode, d=cwd, e=err, o=out))
    return {"stdout": out, "stderr": err}

class Git:
    def __init__(self, repo):
        self.__repo__ = repo
        
    def fetch(self):
        return call("git fetch", self.__repo__)
    
    def merge(self, branch):
        return call("git merge {b}".format(b=branch), self.__repo__)
    
    def checkout(self, branch):
        return call("git checkout {b}".format(b=branch), self.__repo__)
    
    def ls_remote_heads(self, remote="origin"):
        return call("git ls-remote --heads {r}".format(r=remote), self.__repo__)
    
    def rev_list(self, src, dst):
        return call("git rev-list {s}..{d}".format(s=src, d=dst), self.__repo__)

def jsdoc_cli(dst_path):
    with open(CONF.jsdoc_generate_cmd_file, "r") as f:
        for l in f.readlines():
            line = l.lstrip()
            if line.startswith("jsdoc"):
                dst_full = line[line.index("-d"):]
                dst_p = dst_full.split((" "))[1].split(" ")[0]
                return line.replace(dst_p, dst_path)

def generate():
    print("using:\n" + call("jsdoc --version")["stdout"] + call("git --version")["stdout"] + call("rsync --version")["stdout"])
    git = Git(CONF.repo_working_dir)
    git.fetch()
    def filter_branches(branches):
        if len(CONF.branches) == 0:
            return branches
        def out_file(s):
            for i in CONF.branches:
                if i == s:
                    return True
            return False
        return filter(out_file, branches)
    all_remote_branches = []
    for i in git.ls_remote_heads()["stdout"].splitlines():
        all_remote_branches.append(i.split("refs/heads/")[1])
    filtered_branches = filter_branches(all_remote_branches)
    print(u"working with branches: " + ",".join(filtered_branches))
    generated_releases = []
    for branch in filtered_branches:
        remote_branch = "origin/{branch}".format(branch=branch)
        git.checkout(branch)
        rev_list_result = git.rev_list(branch, remote_branch)
        if len(rev_list_result["stdout"]) > 0:
            git.merge(remote_branch)
        sys.stdout.write(u"generating documentation for branch '{branch}'... ".format(branch=branch))
        sys.stdout.flush()
        release_number = "".join([s for s in branch if s.isdigit()])
        dest_path = os.path.join(CONF.generated_base_path, release_number)
        call(jsdoc_cli(dest_path), CONF.jsdoc_tools_base_path)
        print(u"done! saved to '{p}'".format(p=dest_path))
        generated_releases.append(release_number)
        
    print(u"generated releases: " + ",".join(generated_releases))
    
    template = JINJA2_ENV.get_template("index.html.jinja2")
    html = template.render(releases=[{"text": "Release " + r, "link": "./" + r + "/index.html" } for r in sorted(generated_releases, reverse=True)])
    out_file = open(os.path.join(CONF.generated_base_path, "index.html"), "w")
    out_file.write(html.encode("utf8"))
    out_file.close()

def upload():
    print(u"uploading generated files")
    res = call("rsync -av {local}/ {user}@{server}:{remote}".format(local=CONF.generated_base_path, user=CONF.remote_ssh_user, server=CONF.remote_ssh_server, remote=CONF.remote_ssh_dir))
    print(res["stdout"])
    if CONF.delete_generated_after_upload:
        shutil.rmtree(CONF.generated_base_path, ignore_errors=True) 

def main():
    try:
        generate()
        if len(sys.argv) == 2 and sys.argv[1] == "--no-upload":
            print(u"upload skipped")
        else:
            upload()
    except Exception as e:
        sys.exit(u"Error: " + e.message + "\n\n" + traceback.format_exc())
    sys.exit(0)

if __name__ == "__main__":
    main()